# Useful Docker commands


```
# build Docker
docker build -f Dockerfile .
# tag
docker tag 39f4c33fc03e bioinformant/nextflow_and_docker_demo:0.1
# list images on system
docker images
# interact with container
docker run -it --rm bioinformant/nextflow_and_docker_demo:0.1 /bin/bash
```

# Running Nextflow

 * Parameters for running Nextflow are specified with `-` and parameters for the workflow are specified with `--`
 * `-resume` ensures that previously run steps are cached
 * `-ansi-log false` shows every single process that is run, rather than grouping by process and showing % of tasks of that process group that have completed
 * `-with-report` generates a html report called `report.html` by default

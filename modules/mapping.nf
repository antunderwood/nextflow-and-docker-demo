params.output_dir = false

process INDEX_REFERENCE {
  tag "prepare reference"

  input:
    file(reference)
  output:
    path("index")

  script:
    """
    mkdir index
    bwa index ${reference} -p index/reference
    """

}

 process BWA_MAP {
  tag "$id"

  input:
    tuple(val(id), path(reads))
    path(reference_index_dir)
  output:
    tuple(val(id), path("${id}.sorted.bam"))
  script:
    """
    bwa mem ${reference_index_dir}/reference ${reads[0]} ${reads[1]} \
    | samtools view -bS -F 4 - \
    | samtools sort -O bam -o ${id}.sorted.bam
    """
}

process CALL_VARIANTS{
  tag "$id"
  publishDir "${params.output_dir}/bcf_files", mode: "copy"

  input:
    tuple(val(id), path(sorted_bam))
    path(reference)
  output:
    path("${id}.bcf"), emit: bcf_files
  script:
    """
    bcftools mpileup -a DP,AD,SP,ADF,ADR,INFO/AD,INFO/ADF,INFO/ADR \
    -f ${reference} ${sorted_bam} \
    | bcftools call --ploidy 1 -m -Ob -o ${id}.bcf
    """
}


FROM continuumio/miniconda3
LABEL authors="Anthony Underwood" \
      description="Docker image for demo"

RUN apt update; apt install -y gcc bc procps
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a

ENV PATH /opt/conda/envs/simple-mapping/bin:$PATH
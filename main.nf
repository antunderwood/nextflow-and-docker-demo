nextflow.enable.dsl=2

params.reads = false
params.reference = false
params.output_dir = false

include { INDEX_REFERENCE; BWA_MAP; CALL_VARIANTS } from './modules/mapping.nf'

if (params.reads && params.reference && params.output_dir ){
  workflow {
    reads = Channel
    .fromFilePairs(params.reads)
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
    
    INDEX_REFERENCE(file(params.reference))
    BWA_MAP(reads, INDEX_REFERENCE.out)
    CALL_VARIANTS(BWA_MAP.out, file(params.reference))
    // show output files for debugging
    CALL_VARIANTS.out.bcf_files.view()
  }
} else {
    error "Please specify reads, a reference and an output directory with --reads, --reference and --output_dir"
}
